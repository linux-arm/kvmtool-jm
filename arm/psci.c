/* SPDX-License-Identifier: GPL-2.0 WITH Linux-syscall-note */
/* Copyright (C) 2023 ARM Limited */

#include "kvm/kvm.h"
#include <kvm/fdt.h>
#include <kvm/kvm-cpu.h>

#include "arm-common/psci.h"

#include <linux/bitops.h>
#include <linux/bits.h>
#include <linux/kernel.h>
#include <linux/kvm.h>
#include <linux/sizes.h>

struct psci_fns {
	u32 cpu_suspend;
	u32 cpu_off;
	u32 cpu_on;
	u32 migrate;
};

static u64 midr_to_cpu_nr(struct kvm *kvm, u64 midr)
{
	u32 cpu_id = midr;

	if (cpu_id > (u32)kvm->nrcpus)
		return -EINVAL;
	return cpu_id;
}

static void set_x0(struct kvm_cpu *vcpu, u64 val)
{
	struct kvm_one_reg reg;

	reg.addr = (u64)&val;
        reg.id  = ARM64_CORE_REG(regs.regs[0]);
        if (ioctl(vcpu->vcpu_fd, KVM_SET_ONE_REG, &reg) < 0)
                die_perror("KVM_SET_ONE_REG failed (x0])");
}

static DEFINE_MUTEX(psci_cpu_on_off_lock);

static void psci_wait_for_wakeup(struct kvm_cpu *vcpu, void *data)
{
	while (!vcpu->psci_cpu_on) {
		mutex_lock(&psci_cpu_on_off_lock);
		pthread_cond_wait(&vcpu->psci_cpu_on_off_cond,
					&psci_cpu_on_off_lock.mutex);
		mutex_unlock(&psci_cpu_on_off_lock);
	}

	kvm_cpu__reset_vcpu(vcpu);
}

static void mask_32bit_arg(bool is_32_bit, u64 *arg)
{
	if (is_32_bit)
		*arg = *arg & GENMASK(31, 0);
}

static void do_cpu_suspend(struct kvm_cpu *vcpu, u32 power_state)
{
	bool is_32_bit;
	struct kvm_mp_state state;
	u32 state_id = power_state & GENMASK(27, 0);
	bool powerdown = power_state & BIT(30);

	is_32_bit = vcpu->kvm_run->hypercall.args[0] & PSCI_0_2_64BIT;

	if (!state_id) {
		set_x0(vcpu, PSCI_RET_SUCCESS);
	} else if (!powerdown) {
		struct kvm_mp_state state;

		state.mp_state = KVM_MP_STATE_SUSPENDED;
		ioctl(vcpu->vcpu_fd, KVM_SET_MP_STATE, &state);
		set_x0(vcpu, PSCI_RET_SUCCESS);
	} else {
		u64 entry_point_address = vcpu->kvm_run->hypercall.args[2];
		u32 context_id = vcpu->kvm_run->hypercall.args[3];

		mask_32bit_arg(is_32_bit, &entry_point_address);

		vcpu->cpu_on_entry_point = entry_point_address;
		vcpu->cpu_on_context_id = context_id;

		/*
		 * Reset the vCPU after waking from suspend. This ensures the
		 * timer fires, before VCPU_INIT disables it.
		 */
		vcpu->needs_cpu_reset = 1;
		state.mp_state = KVM_MP_STATE_SUSPENDED;
		if (ioctl(vcpu->vcpu_fd, KVM_SET_MP_STATE, &state)) {
			/* Failed to sleep - do the reset now */
			kvm_cpu__reset_vcpu(vcpu);
			vcpu->needs_cpu_reset = 0;
		}
	}
}

bool kvm_handle_psci_call(struct kvm_cpu *vcpu)
{
	bool is_32_bit = false;
	struct kvm *kvm = vcpu->kvm;

	is_32_bit = vcpu->kvm_run->hypercall.args[0] & PSCI_0_2_64BIT;

	switch (vcpu->kvm_run->hypercall.args[0]) {
	case PSCI_0_2_FN_PSCI_VERSION:
		set_x0(vcpu, PSCI_VERSION(1, 2));
		return true;
	case PSCI_0_2_FN_CPU_SUSPEND:
	case PSCI_0_2_FN64_CPU_SUSPEND: {
		u32 power_state = vcpu->kvm_run->hypercall.args[1];

		if (!kvm->cfg.arch.cpususpend)
			set_x0(vcpu, PSCI_RET_SUCCESS);
		else
			do_cpu_suspend(vcpu, power_state);

		/* Don't modify PC/X0 values. */

		return true;
	}
	case PSCI_0_2_FN_CPU_OFF:
		vcpu->psci_cpu_on = 0;
		psci_wait_for_wakeup(vcpu, NULL);

		/* Don't modify PC/X0 values. */

		return true;
	case PSCI_0_2_FN64_CPU_ON:
	case PSCI_0_2_FN_CPU_ON: {
		u32 target_cpu_midr = vcpu->kvm_run->hypercall.args[1];
		u32 target_cpu_nr = midr_to_cpu_nr(kvm, target_cpu_midr);
		u64 entry_point_address = vcpu->kvm_run->hypercall.args[2];
		u64 context_id = vcpu->kvm_run->hypercall.args[3];
		struct kvm_cpu *target_vcpu;

		mask_32bit_arg(is_32_bit, &entry_point_address);
		mask_32bit_arg(is_32_bit, &context_id);

		if (target_cpu_nr < 0) {
			set_x0(vcpu, PSCI_RET_INVALID_PARAMS);
			return true;
		}

		if (!guest_flat_to_host(kvm, entry_point_address)) {
			set_x0(vcpu, PSCI_RET_INVALID_ADDRESS);
			return true;
		}

		target_vcpu = kvm->cpus[target_cpu_nr];
		if (target_vcpu->psci_cpu_on) {
			set_x0(vcpu, PSCI_RET_ALREADY_ON);
			return true;
		}

		target_vcpu->cpu_on_entry_point = entry_point_address;
		target_vcpu->cpu_on_context_id = context_id;
		target_vcpu->psci_cpu_on = 1;

		mutex_lock(&psci_cpu_on_off_lock);
		pthread_cond_signal(&target_vcpu->psci_cpu_on_off_cond);
		mutex_unlock(&psci_cpu_on_off_lock);
		set_x0(vcpu, PSCI_RET_SUCCESS);

		return true;
	}
	case PSCI_0_2_FN64_AFFINITY_INFO:
	case PSCI_0_2_FN_AFFINITY_INFO: {
		u64 target_affinity = vcpu->kvm_run->hypercall.args[1];
		/* lowest_affinity_level is assumed to be 0 for PSCI v1 */
		u32 target_cpu_nr = midr_to_cpu_nr(kvm, target_affinity);
		struct kvm_cpu *target_vcpu;

		mask_32bit_arg(is_32_bit, &target_affinity);

		if (target_cpu_nr < 0) {
			set_x0(vcpu, PSCI_RET_INVALID_PARAMS);
			return true;
		}

		target_vcpu = kvm->cpus[target_cpu_nr];
		if (!target_vcpu->psci_cpu_on) {
			set_x0(vcpu, PSCI_0_2_AFFINITY_LEVEL_OFF);
			return true;
		} else {
			set_x0(vcpu, PSCI_0_2_AFFINITY_LEVEL_ON);
			return true;
		}
	}
	case PSCI_0_2_FN_SYSTEM_OFF:
	case PSCI_0_2_FN_SYSTEM_RESET:
		kvm__reboot(kvm);
		return true;
	case PSCI_1_0_FN_PSCI_FEATURES: {
		u32 psci_func_id = vcpu->kvm_run->hypercall.args[1];

		switch (psci_func_id) {
		case PSCI_0_2_FN_PSCI_VERSION:
		case PSCI_0_2_FN_CPU_OFF:
		case PSCI_0_2_FN64_CPU_ON:
		case PSCI_0_2_FN_CPU_ON:
		case PSCI_0_2_FN64_AFFINITY_INFO:
		case PSCI_0_2_FN_AFFINITY_INFO:
		case PSCI_0_2_FN_SYSTEM_OFF:
		case PSCI_0_2_FN_SYSTEM_RESET:
		case PSCI_1_0_FN_PSCI_FEATURES:
			set_x0(vcpu, 0);
			break;
		case PSCI_0_2_FN_CPU_SUSPEND:
		case PSCI_0_2_FN64_CPU_SUSPEND:
			/*
			 * bit-1: The extended state-id format is used.
			 * See 5.4.2.2 "Extended StateId Format" of DEN0022.E
			 */
			set_x0(vcpu, BIT(1));
			break;
		default:
			set_x0(vcpu, PSCI_RET_NOT_SUPPORTED);
			break;
		}
		return true;
	}
	default:
		set_x0(vcpu, PSCI_RET_NOT_SUPPORTED);
		return true;
	}
}

static int kvm_arch_has_hvc_to_user(struct kvm *kvm)
{
	int ret;

	ret = ioctl(kvm->sys_fd, KVM_CHECK_EXTENSION, KVM_CAP_ARM_HVC_TO_USER);
	if (ret <= 0)
		ret = 0;

	return ret;
}

static int kvm_arch_enable_hvc_to_user(struct kvm *kvm)
{
	struct kvm_enable_cap cap = {
		.cap = KVM_CAP_ARM_HVC_TO_USER,
	};

	if (!kvm_arch_has_hvc_to_user(kvm))
		return -EOPNOTSUPP;

	if (ioctl(kvm->vm_fd, KVM_ENABLE_CAP, &cap))
		die_perror("KVM_ENABLE_CAP(KVM_CAP_ARM_HVC_TO_USER)");

	return 0;
}

static int kvm_arch_has_psci_to_user(struct kvm *kvm)
{
	int ret;

	ret = ioctl(kvm->sys_fd, KVM_CHECK_EXTENSION, KVM_CAP_ARM_PSCI_TO_USER);
	if (ret <= 0)
		ret = 0;

	return ret;
}

static int kvm_arch_enable_psci_to_user(struct kvm *kvm)
{
	struct kvm_enable_cap cap = {
		.cap = KVM_CAP_ARM_PSCI_TO_USER,
	};

	if (!kvm_arch_has_psci_to_user(kvm))
		return -EOPNOTSUPP;

	if (ioctl(kvm->vm_fd, KVM_ENABLE_CAP, &cap))
		die_perror("KVM_ENABLE_CAP(KVM_CAP_ARM_PSCI_TO_USER)");

	return 0;
}

void kvm__arch_psci_init(struct kvm *kvm)
{
	if (!kvm_arch_has_psci_to_user(kvm))
		return;

	if (kvm_arch_enable_hvc_to_user(kvm))
		return;

	if (kvm_arch_enable_psci_to_user(kvm))
		return;

	kvm->arch.psci_to_user = true;
}

struct kvm_cpu_task psci_sleep_task;

static int kvm__arch_psci_vcpu_init(struct kvm *kvm)
{

	if (!kvm->arch.psci_to_user)
		return 0;

	kvm->cpus[0]->psci_cpu_on = 1;

	/* vCPUs block in user-space waiting for CPU_ON */
	psci_sleep_task.func = psci_wait_for_wakeup;
	psci_sleep_task.data = NULL;
	kvm_cpu__queue_task_on_all_cpus(kvm, &psci_sleep_task);

	return 0;
}
late_init(kvm__arch_psci_vcpu_init);

static struct psci_fns psci_0_1_fns = {
	.cpu_suspend = KVM_PSCI_FN_CPU_SUSPEND,
	.cpu_off = KVM_PSCI_FN_CPU_OFF,
	.cpu_on = KVM_PSCI_FN_CPU_ON,
	.migrate = KVM_PSCI_FN_MIGRATE,
};

static struct psci_fns psci_0_2_aarch32_fns = {
	.cpu_suspend = PSCI_0_2_FN_CPU_SUSPEND,
	.cpu_off = PSCI_0_2_FN_CPU_OFF,
	.cpu_on = PSCI_0_2_FN_CPU_ON,
	.migrate = PSCI_0_2_FN_MIGRATE,
};

static struct psci_fns psci_0_2_aarch64_fns = {
	.cpu_suspend = PSCI_0_2_FN64_CPU_SUSPEND,
	.cpu_off = PSCI_0_2_FN_CPU_OFF,
	.cpu_on = PSCI_0_2_FN64_CPU_ON,
	.migrate = PSCI_0_2_FN64_MIGRATE,
};

void kvm__arch_psci_setup_fdt(void *fdt, struct kvm *kvm)
{
	struct psci_fns *fns;

	_FDT(fdt_begin_node(fdt, "psci"));
	if (kvm->arch.psci_to_user ||
	    kvm__supports_extension(kvm, KVM_CAP_ARM_PSCI_0_2)) {
		const char compatible[] = "arm,psci-0.2\0arm,psci";
		_FDT(fdt_property(fdt, "compatible",
				  compatible, sizeof(compatible)));
		if (kvm->cfg.arch.aarch32_guest)
			fns = &psci_0_2_aarch32_fns;
		else
			fns = &psci_0_2_aarch64_fns;
	} else {
		_FDT(fdt_property_string(fdt, "compatible", "arm,psci"));
		fns = &psci_0_1_fns;
	}
	_FDT(fdt_property_string(fdt, "method", "hvc"));
	_FDT(fdt_property_cell(fdt, "cpu_suspend", fns->cpu_suspend));
	_FDT(fdt_property_cell(fdt, "cpu_off", fns->cpu_off));
	_FDT(fdt_property_cell(fdt, "cpu_on", fns->cpu_on));
	_FDT(fdt_property_cell(fdt, "migrate", fns->migrate));
	_FDT(fdt_end_node(fdt));

	if (kvm->arch.psci_to_user && kvm->cfg.arch.cpususpend) {
		_FDT(fdt_begin_node(fdt, "idle-states"));
		_FDT(fdt_property_string(fdt, "entry-method", "psci"));

		_FDT(fdt_begin_node(fdt, "cpu-sleep-0"));
		_FDT(fdt_property_cell(fdt, "phandle", PHANDLE_CPU_SLEEP0));
		_FDT(fdt_property_string(fdt, "compatible", "arm,idle-state"));
		_FDT(fdt_property_cell(fdt, "arm,psci-suspend-param", 0));

		// These numbers are meaningless for a guest
		_FDT(fdt_property_cell(fdt, "entry-latency-us", 1000));
		_FDT(fdt_property_cell(fdt, "exit-latency-us", 1000));
		_FDT(fdt_property_cell(fdt, "min-residency-us", 10000));
		_FDT(fdt_end_node(fdt));

		_FDT(fdt_begin_node(fdt, "cpu-sleep-1"));
		_FDT(fdt_property_cell(fdt, "phandle", PHANDLE_CPU_SLEEP1));
		_FDT(fdt_property_string(fdt, "compatible", "arm,idle-state"));
		_FDT(fdt_property_cell(fdt, "arm,psci-suspend-param", 1));
		_FDT(fdt_property_cell(fdt, "entry-latency-us", 1000));
		_FDT(fdt_property_cell(fdt, "exit-latency-us", 1000));
		_FDT(fdt_property_cell(fdt, "min-residency-us", 20000));
		_FDT(fdt_end_node(fdt));

		_FDT(fdt_begin_node(fdt, "cpu-sleep-2"));
		_FDT(fdt_property_cell(fdt, "phandle", PHANDLE_CPU_SLEEP2));
		_FDT(fdt_property_string(fdt, "compatible", "arm,idle-state"));
		_FDT(fdt_property_cell(fdt, "arm,psci-suspend-param", BIT(30)));
		_FDT(fdt_property_cell(fdt, "entry-latency-us", 1000));
		_FDT(fdt_property_cell(fdt, "exit-latency-us", 1000));
		_FDT(fdt_property_cell(fdt, "min-residency-us", 30000));
		_FDT(fdt_end_node(fdt));

		_FDT(fdt_begin_node(fdt, "cpu-sleep-3"));
		_FDT(fdt_property_cell(fdt, "phandle", PHANDLE_CPU_SLEEP3));
		_FDT(fdt_property_string(fdt, "compatible", "arm,idle-state"));
		_FDT(fdt_property_cell(fdt, "arm,psci-suspend-param", BIT(30) | 1));
		_FDT(fdt_property_cell(fdt, "entry-latency-us", 1000));
		_FDT(fdt_property_cell(fdt, "exit-latency-us", 1000));
		_FDT(fdt_property_cell(fdt, "min-residency-us", 30000));
		_FDT(fdt_end_node(fdt));

		_FDT(fdt_end_node(fdt));
	} else if (!kvm->arch.psci_to_user && kvm->cfg.arch.cpususpend) {
		pr_warning("Host kernel does not support HVC/PSCI to user-space, --cpususpend option ignored.");
	}
}

void kvm__arch_psci_cpu_suspend_setup_fdt(void *fdt, struct kvm_cpu *vcpu)
{
	u32 phandles[4];
	struct kvm *kvm = vcpu->kvm;

	if (kvm->arch.psci_to_user && kvm->cfg.arch.cpususpend) {
		phandles[0] = cpu_to_fdt32(PHANDLE_CPU_SLEEP0);
		phandles[1] = cpu_to_fdt32(PHANDLE_CPU_SLEEP1);
		phandles[2] = cpu_to_fdt32(PHANDLE_CPU_SLEEP2);
		phandles[3] = cpu_to_fdt32(PHANDLE_CPU_SLEEP3);
		_FDT(fdt_property(fdt, "cpu-idle-states",
				  (void *)&phandles, sizeof(phandles)));
	}
}
