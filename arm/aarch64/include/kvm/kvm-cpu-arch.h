#ifndef KVM__KVM_CPU_ARCH_H
#define KVM__KVM_CPU_ARCH_H

#include "kvm/kvm.h"

#include "arm-common/kvm-cpu-arch.h"

#define ARM_MPIDR_HWID_BITMASK	0xFF00FFFFFFUL
#define ARM_CPU_ID		3, 0, 0, 0
#define ARM_CPU_ID_MPIDR	5
#define ARM_CPU_CTRL		3, 0, 1, 0
#define ARM_CPU_CTRL_SCTLR_EL1	0

static inline __u64 __core_reg_id(__u64 offset)
{
        __u64 id = KVM_REG_ARM64 | KVM_REG_ARM_CORE | offset;

        if (offset < KVM_REG_ARM_CORE_REG(fp_regs))
                id |= KVM_REG_SIZE_U64;
        else if (offset < KVM_REG_ARM_CORE_REG(fp_regs.fpsr))
                id |= KVM_REG_SIZE_U128;
        else
                id |= KVM_REG_SIZE_U32;

        return id;
}
#define ARM64_CORE_REG(x) __core_reg_id(KVM_REG_ARM_CORE_REG(x))

void kvm_cpu__select_features(struct kvm *kvm, struct kvm_vcpu_init *init);
int kvm_cpu__configure_features(struct kvm_cpu *vcpu);
int kvm_cpu__setup_pvtime(struct kvm_cpu *vcpu);
int kvm_cpu__teardown_pvtime(struct kvm *kvm);

#endif /* KVM__KVM_CPU_ARCH_H */
