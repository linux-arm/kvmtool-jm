/* SPDX-License-Identifier: GPL-2.0 */
/* Copyright (C) 2023 ARM Limited */
#ifndef ARM_COMMON__PSCI_H
#define ARM_COMMON__PSCI_H

#include <kvm/kvm-cpu.h>

#define SMCC_CC_FUNCTION_NUMBER 0x0000FFFF

static inline bool kvm_is_psci_call(struct kvm_cpu *vcpu)
{
	u64 mask = PSCI_0_2_64BIT | SMCC_CC_FUNCTION_NUMBER;

	if (vcpu->kvm_run->hypercall.nr != 0)
		return false;

	if ((vcpu->kvm_run->hypercall.args[0] & ~mask) != PSCI_0_2_FN_BASE)
		return false;

	return true;
}

void kvm__arch_psci_init(struct kvm *kvm);
void kvm__arch_psci_setup_fdt(void *fdt, struct kvm *kvm);
void kvm__arch_psci_cpu_suspend_setup_fdt(void *fdt, struct kvm_cpu *vcpu);
bool kvm_handle_psci_call(struct kvm_cpu *vcpu);

#endif /* ARM_COMMON__PSCI_H */
